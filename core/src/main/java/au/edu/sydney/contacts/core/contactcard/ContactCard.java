package au.edu.sydney.contacts.core.contactcard;

import java.util.ArrayList;
import java.util.List;

public class ContactCard {
	private String contactFullName;
	private String contactRole;
	private List<String> phoneNumbers = new ArrayList<String>();
	private List<String> emails = new ArrayList<String>();
	public String getContactFullName() {
		return contactFullName;
	}
	public void setContactFullName(String contactFullName) {
		this.contactFullName = contactFullName;
	}
	public String getContactRole() {
		return contactRole;
	}
	public void setContactRole(String contactRole) {
		this.contactRole = contactRole;
	}
	public List<String> getPhoneNumbers() {
		return phoneNumbers;
	}
	public void setPhoneNumbers(List<String> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}
	public List<String> getEmails() {
		return emails;
	}
	public void setEmails(List<String> emails) {
		this.emails = emails;
	}
}
