package au.edu.sydney.contacts.core.contactcard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;

import com.adobe.cq.sightly.WCMUse;
public class ContactCardComponent extends WCMUse{
	
	private ContactCard contactCard=null;
	private String error=null;


	@Override
	public void activate() throws Exception {
		try{
			String path = getProperties().get("path", "Default Title");   
	        ResourceResolver resourceResolver = getResourceResolver();
	        if (path!=null && !path.isEmpty()) {
	            Resource contactResource = resourceResolver.getResource(path+"/jcr:content/profile");
	            if (contactResource != null) {
	            	Node contactNode = contactResource.adaptTo(Node.class);
	            	contactCard = extractContactCard(contactNode);
	            }
	        }
		} catch(Exception e){
			e.printStackTrace();
			error = e.getMessage();
		}
	}
	
	private ContactCard extractContactCard(Node contact) throws ValueFormatException, PathNotFoundException, RepositoryException{
		ContactCard card= new ContactCard();
		
		StringBuilder contactFullName = new StringBuilder();    	
    	contactFullName.append(contact.hasProperty("title")?
    			contact.getProperty("title").getString().concat(" "):"");
    	
    	contactFullName.append(contact.hasProperty("name")?
    			contact.getProperty("name").getString():"");
    	
    	card.setContactFullName(contactFullName.toString());
    	
    	StringBuilder contactRole = new StringBuilder();    	
    	contactRole.append(contact.hasProperty("position")?
    			contact.getProperty("position").getString().concat(" "):"");
    	
    	contactRole.append(contact.hasProperty("department")?
    			contact.getProperty("department").getString():"");
    	card.setContactRole(contactRole.toString());
    	
    	if (contact.hasProperty("phone")) {
    		List<String> phoneNumbers = new ArrayList<String>();  
    		
            List<Value> phoneValues = Arrays.asList(contact.getProperty("phone").getValues());
            for (Value phoneValue : phoneValues) {
            	phoneNumbers.add(phoneValue.getString());
            }
            card.setPhoneNumbers(phoneNumbers);
    	}

    	if (contact.hasProperty("email")) {
    		List<String> emails = new ArrayList<String>();  
            List<Value> emailValues = Arrays.asList(contact.getProperty("email").getValues());
            for (Value emailValue : emailValues) {
            	emails.add(emailValue.getString());
            }
            card.setEmails(emails);
    	}
    	return card;
	}

	public ContactCard getContactCard() {
		return contactCard;
	}
	
	public String getError(){
		return error;
	}
}
